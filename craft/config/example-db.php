<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'harvested.dev' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
    'harvested.co.nz' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
);
