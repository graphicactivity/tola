<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
    ),

    'tola.dev' => array(
        'devMode' => true,
        'allowAutoUpdates' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/tola.dev/public/',
            'baseUrl'  => 'http://tola.dev/',
        )
    ),

    'tola.co' => array(
        'devMode' => false,
        'allowAutoUpdates' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://tola.co/',
        )
    )
);
